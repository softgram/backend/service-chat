package co.com.project.chat.adapter.inbound.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(builderMethodName = "init")
public class EntityBaseDTO {
    private Long id;
    private LocalDate createAt;
    private LocalDate updateAt;
    private Boolean status;
}
