package co.com.project.chat.adapter.inbound.web.controller;

import co.com.project.chat.adapter.inbound.mapper.MessageMapStructDTO;
import co.com.project.chat.adapter.inbound.web.dto.MessageDTO;
import co.com.project.chat.application.port.ChatUseCase;
import co.com.project.common.adapter.inbound.web.dto.Response;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import static co.com.project.common.adapter.util.ResponseBuilder.success;

@Controller
@CrossOrigin(originPatterns = {"http://localhost:3000"})
@RequiredArgsConstructor
public class MessageController {
    private final MessageMapStructDTO mapper;
    private final ChatUseCase service;

    @MessageMapping("/chat/connect")
    @SendTo("/topic/message")
    public Response<MessageDTO> message(MessageDTO message) {
        return success(mapper.toDTO(service.save(mapper.toDomain(message))));
    }
}
