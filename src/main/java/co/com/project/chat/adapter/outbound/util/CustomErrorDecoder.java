package co.com.project.chat.adapter.outbound.util;

import co.com.project.chat.domain.exception.ClientRequestException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class CustomErrorDecoder implements ErrorDecoder {
    @Override
    public Exception decode(String methodKey, Response response) {
        return new ClientRequestException(response.reason());
    }
}
