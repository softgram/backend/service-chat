package co.com.project.chat.adapter.outbound.feign.core;

import co.com.project.chat.adapter.configuration.FeignConfiguration;
import co.com.project.chat.adapter.inbound.web.dto.MessageDTO;
import co.com.project.common.adapter.inbound.web.dto.Response;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "service-core", path = "/api/messages", configuration = FeignConfiguration.class)
public interface CoreClient {
    @PostMapping
    Response<MessageDTO> saveMessage(@RequestBody MessageDTO message);
}
