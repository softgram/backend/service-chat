package co.com.project.chat.adapter.inbound.web.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Null;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuperBuilder(builderMethodName = "init")
public class ChatDTO extends EntityBaseDTO {
    @NotNull
    @NotEmpty
    private Long userId;
    @NotNull
    @NotEmpty
    private Long otherId;
    @Null
    private List<MessageDTO> messages;
}
