package co.com.project.chat.adapter.outbound.persistance.repository;

import co.com.project.chat.adapter.inbound.mapper.MessageMapStructDTO;
import co.com.project.chat.adapter.outbound.feign.core.CoreClient;
import co.com.project.chat.domain.model.Message;
import co.com.project.chat.domain.repository.ChatRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class ChatRepositoryImpl implements ChatRepository {
    private final MessageMapStructDTO mapper;
    private final CoreClient client;

    @Override
    public Message save(Message message) {
        return mapper.toDomain(client.saveMessage(mapper.toDTO(message)).getData());
    }
}
