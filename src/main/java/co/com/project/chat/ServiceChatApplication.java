package co.com.project.chat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication(
    scanBasePackages = {
        "co.com.project.chat",
        "co.com.project.common"
    }
)
public class ServiceChatApplication {

    public static void main(String... args) {
        SpringApplication.run(ServiceChatApplication.class);
    }

}
