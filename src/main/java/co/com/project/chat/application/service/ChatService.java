package co.com.project.chat.application.service;

import co.com.project.chat.application.port.ChatUseCase;
import co.com.project.chat.domain.model.Message;
import co.com.project.chat.domain.repository.ChatRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ChatService implements ChatUseCase {
    private final ChatRepository repository;

    @Override
    public Message save(Message message) {
        return repository.save(message);
    }
}
