package co.com.project.chat.application.port;

import co.com.project.chat.domain.model.Message;

public interface ChatUseCase {
    Message save(Message message);
}
