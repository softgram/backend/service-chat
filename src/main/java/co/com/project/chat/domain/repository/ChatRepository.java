package co.com.project.chat.domain.repository;

import co.com.project.chat.domain.model.Message;

public interface ChatRepository {
    Message save(Message message);
}
