FROM openjdk:20 as builder

WORKDIR /app

COPY ./build/libs/service-chat-1.0-SNAPSHOT.jar .

ENTRYPOINT java -jar ./service-chat-1.0-SNAPSHOT.jar